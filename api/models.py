from sqlalchemy import Column, Integer, String, DateTime
from database import Base
import datetime


class Event(Base):
    __tablename__ = "events"
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer)
    event_name = Column(String(256))
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
