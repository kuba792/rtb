from typing import List
from fastapi import FastAPI, status, Depends
from database import Base, engine, SessionLocal
from sqlalchemy.orm import Session
import models
import schemas
from fastapi.middleware.cors import CORSMiddleware

# Create the database tables
Base.metadata.create_all(engine)

app = FastAPI()


def get_session():
    session = SessionLocal()
    try:
        yield session
    finally:
        session.close()


@app.post("/events", response_model=schemas.Event, status_code=status.HTTP_201_CREATED)
def create_event(event: schemas.EventCreate, session: Session = Depends(get_session)):
    db_event = models.Event(user_id=event.user_id, event_name=event.event_name)

    session.add(db_event)
    session.commit()
    session.refresh(db_event)

    return db_event


@app.get("/stats", response_model=dict)
def read_stats(session: Session = Depends(get_session)):
    events_list = session.query(models.Event).all()

    unique_users = len(
        set([event.user_id for event in events_list if event.event_name == "page_load"])
    )
    avatar_view = [event for event in events_list if event.event_name == "avatar_view"]
    avatar_view = len(set([event.user_id for event in avatar_view]))
    avatar_view = (avatar_view / unique_users) * 100

    return {
        "unique_users": unique_users,
        "avatar_view": avatar_view,
    }
