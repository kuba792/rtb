from pydantic import BaseModel
import datetime


class EventCreate(BaseModel):
    user_id: int
    event_name: str


class Event(EventCreate):
    id: int
    created_at: datetime.datetime

    class Config:
        orm_mode = True
