function sendEvent(user_id, event_name) {
    fetch('/events', {
        method: 'POST',
        body: JSON.stringify({
            user_id: user_id,
            event_name: event_name,
        }),
        headers: {
            'Content-type': 'application/json',
        },
    });
}

function placeAvatar(url) {
    const paragraph = document.querySelector('p:nth-of-type(10)');
    const image = document.createElement('img');
    image.id = 'avatar';
    image.src = url;
    image.alt = 'avatar';

    // @todo: this may cause CLS issue
    // use placeholder before image is loaded
    paragraph.after(image);
}

function isElementInViewport(el) {
    var rect = el.getBoundingClientRect();

    return (
        rect.top <= (window.innerHeight || document.documentElement.clientHeight) &&
        rect.bottom >= 0
    );
}

function detectAvatarView(user_id) {
    function handleScroll() {
        var avatar = document.getElementById("avatar");

        if (isElementInViewport(avatar)) {
            sendEvent(user_id, 'avatar_view');
            window.removeEventListener("scroll", handleScroll);
        }
    }

    window.addEventListener("scroll", handleScroll);
}


function handleUserData(user, save = true) {
    if (save) {
        localStorage.setItem('user', JSON.stringify(user));
    }

    sendEvent(user.id, 'page_load');
    try {
        placeAvatar(user.avatar);
        detectAvatarView(user.id);
    } catch (e) {
        console.log("could not place avatar", e);
    }
}


const cached_user = JSON.parse(localStorage.getItem('user'));

if (cached_user) {
    handleUserData(cached_user, false);
} else
    fetch('https://random-data-api.com/api/v2/users')
        .then(response => response.json())
        .then(handleUserData);
