# rtb



## Getting started

Run with docker-compose 

```
docker-compose up -d
```

The service will be available at http://localhost:8080

## Decisions
I don't know how big the project is going to be, so I decided to do it as simple as possible to make it easy to scale in the future.

### Frontend
I decided to build the frontend on basic html files with a little bit of JS.
In such simple project engaging any framework would be an overkill.

With this approach, the performance and SEO is perfect. Page loads instantly, and works even without JS enabled.

For now, static files are served directly from Nginx. In a real project I would use a CDN for that.

### Database
In this case I decided to use SQLite. It's a simple file based database, which is easy to use and doesn't require any additional setup.
I a small case it's usually enough, and it's easy to migrate to a more powerful database if needed.

In a larger project I would use PostgreSQL, which is a powerful and reliable database.

In even bigger scale I would use a NoSQL database, which is easy to scale horizontally. Also it's easier to handle huge amounts of write operations leveraging eventual consistency, which is always a case in tracking systems.

### Backend
I decided to use Python with FastAPI framework. It's a simple and lightweight framework, offering great performance and scalability. Additionally, it comes with a built-in OpenAPI UI, which is a great tool for testing and documentation.